import { Component, OnInit } from '@angular/core';

import { VideoService } from '../services/video.service'

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css'],
})
export class SearchBarComponent implements OnInit {

  url : string;

  //searchBox = document.getElementById("search-box")
  //subscription= fromEvent(this.searchBox, 'keyup')

  constructor(private videoService: VideoService) { }

  ngOnInit(): void {
  }

  search(url: string){
    this.url = url;
    //console.log("OK");
    this.videoService.search(url, false);
    
  }

}   
