import { Component, OnInit, OnDestroy} from '@angular/core';

import { VideoService } from '../services/video.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.css'],
})
export class VideoViewComponent implements OnInit, OnDestroy {

  subscription : Subscription;
  embedUrl: string = '';

  constructor(private videoService : VideoService) {
    //this.subscription = this.videoService.searchTerms.subscribe((url) => {url = this.videoService.transform(url);this.embedUrl = url});  
  }

  ngOnInit(): void {
    this.embedUrl = '../../assets/black-rectangle.png'
    this.getUrl();
  }

  getUrl(){
    this.subscription = this.videoService.view().subscribe((url) =>{
      url = this.transformUrl(url);
      this.embedUrl = url;
    });
  }

  transformUrl(url: string): string { //embed the url and remove time tag
    let transformUrl: string = url.replace("watch?v=", "embed/").replace(/&t=\d*s/, "").trim();
    console.log("transform:"+ transformUrl);
    return transformUrl; 
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
