import { Component, OnInit } from '@angular/core';
import { VideoService } from '../services/video.service'
import { Subscription } from 'rxjs';
import { HistoryService } from '../services/history.service';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  history: string[] = [];


  constructor(private videoService: VideoService, private historyService: HistoryService) { 
    //this.videoService.searchHistory.subscribe((history) => this.history = history);
  }

  ngOnInit(): void {
    this.getHistory()
  }

  getHistory(){
    this.historyService.getHistory().subscribe((history) => this.history = history.reverse())
  }

  browseHistory(url: string): void{
    this.videoService.search(url, true);
  }

  clearHistory(){
    this.historyService.clearHistory();
  }

}
