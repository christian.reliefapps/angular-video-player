import { Component, OnInit } from '@angular/core';
import { BookmarkService } from '../services/bookmark.service';
import { VideoService } from '../services/video.service';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {

  bookmarks : string[] = [];
  currentUrl: string;

  constructor(private bookmarkService: BookmarkService, private videoService: VideoService) { }

  ngOnInit(): void {
    this.bookmarkService.getBookmarks().subscribe((bookmarks) => this.bookmarks = bookmarks);
    this.videoService.view().subscribe((url) => this.currentUrl = url)
  }

  browseBookmarks(url){
    this.videoService.search(url, false)
  }

  addBookmark(){
    let temp = this.bookmarks.indexOf(this.currentUrl);
    console.log(temp);
    if (this.bookmarks.indexOf(this.currentUrl) === -1){
      this.bookmarkService.addBookmarks(this.currentUrl);
    }
  }





}
