import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  private searchHistory$: Subject<string[]> = new Subject<string[]>();

  constructor() {
    localStorage.setItem('history',JSON.stringify([]));
   }

  getHistory(): Observable<string[]>{
    let history: string[] = JSON.parse(localStorage.getItem('history'));
    this.searchHistory$.next(history);
    return this.searchHistory$;
  }

  updateHistory(url: string){
    let history : string[] = JSON.parse(localStorage.getItem('history'));
    history.indexOf(url) !== -1? history.splice(history.indexOf(url),1): '';
    history.push(url);
    console.log(history)
    localStorage.setItem('history', JSON.stringify(history));
    this.searchHistory$.next(history);
  }

  clearHistory(){
    localStorage.setItem('history','[]');
    this.searchHistory$.next([]);
  }

}
