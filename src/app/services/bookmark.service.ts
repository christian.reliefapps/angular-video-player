import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookmarkService {

  private bookmarks$ : Subject<string[]> = new Subject<string[]>();
  
  constructor() { 
    localStorage.setItem('bookmarks','[]');
  }

  getBookmarks(): Subject<string[]>{
    let bookmarks: string[] = JSON.parse(localStorage.getItem('bookmarks'));
    this.bookmarks$.next(bookmarks);
    return this.bookmarks$;
  }

  addBookmarks(url: string): void{
    let bookmarks: string[] = JSON.parse(localStorage.getItem('bookmarks'));
    url ? bookmarks.push(url): '';
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    this.bookmarks$.next(bookmarks);
  }
  
  deleteBookmarks(url: string): void{
    let bookmarks: string[] = JSON.parse(localStorage.getItem('bookmarks'));
    bookmarks.splice(bookmarks.indexOf(url),1);
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    this.bookmarks$.next(bookmarks);
  }


}
