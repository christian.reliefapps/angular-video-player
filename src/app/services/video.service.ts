import { Injectable } from '@angular/core';
import { Subject, Observable, from, of } from 'rxjs';
import { HistoryService } from './history.service';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  searchTerms: Subject<string> = new Subject<string>();

  constructor(private historyService: HistoryService) { 
    
  }

  search(url: string, fromHistory: boolean){
    console.log("search:"+ url);
    if (this.checkUrl(url)) {
      this.searchTerms.next(url.trim());
      fromHistory ? '' : this.historyService.updateHistory(url);
    }
  }

  view(): Subject<string>{
    return this.searchTerms
  }

  checkUrl(url: string): boolean{
    return url.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/) === null ? false: true;
  }

}
